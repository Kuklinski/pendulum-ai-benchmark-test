import numpy as np
import pandas as pd

import tensorflow as tf
from sklearn.model_selection import train_test_split

import os

from timeit import default_timer as timer

def Main():
    folderPRZED = '..' # here add yours path with models
    folderPO = '..'# here add yours path where save models after trening
    LOSS = 1.0 # procent bledu treningu sieci
    EPO_MAX = 1000

    print("Wczytywanie danych treningowych...")
    DANE_IN = pd.read_csv('Input_Vectors.csv', sep=',', header=None)
    TARGET = pd.read_csv('TargetN.csv', sep=',', header=None)

    print("Wczytano.")
    print("")
    print("ILOŚĆ Wektorow sumarycznie: " + str(len(TARGET)))
    print("")
    print("Skalowanie...")
    DANE_IN = DANE_IN.rename(columns={0: "ANGLE", 1: "POSITION", 2: "V_pole", 3: "V_cart"})
    TARGET = TARGET.rename(columns={0: "FORCE"})

    DANE_IN['ANGLE'] = DANE_IN['ANGLE'].div(15)
    DANE_IN['V_pole'] = DANE_IN['V_pole'].div(4)
    DANE_IN['V_cart'] = DANE_IN['V_cart'].div(2)

    TARGET['FORCE'] = TARGET['FORCE'].div(30)
    TARGET['FORCE'] = TARGET['FORCE'].add(0.5)

    print("Przeskalowano do zakresu <0; 1>")
    X = DANE_IN.values
    Y = TARGET.values

    X_tren, x_val, Y_tren, y_val = train_test_split(X, Y, train_size=0.70, random_state=16)

    print("Wektory treningowe: " + str(len(Y_tren)))
    print("Wektory walidacyjne: " + str(len(y_val)))
    print("")

    entries = os.listdir(folderPRZED)

    for ANN in entries:
        modelName = folderPRZED+'/'+ANN
        # wczytanie modelu nietrenowanego

        model = tf.keras.models.load_model(modelName)
        print("Wczytano model sieci:  "+str(ANN))
        print("")
        print("Rozpoczecie treningu...")
        print("")
        czasy = []
        historLoss = []
        epki = 0
        actuall = 100.0

        while (actuall > LOSS) and (epki < EPO_MAX):
            start = timer()
            History = model.fit(X_tren, Y_tren, epochs=200, batch_size=50, verbose=2, validation_data=(x_val, y_val))
            historLoss.extend(History.history['loss'])
            ost = len(History.history['loss'])
            actuall = History.history['loss'][ost - 1]
            end = timer()
            epki = epki + 200
            czasy.append(end - start)

        print("")
        print("")
        print("===================================================")
        print("CZAS NAUKI: " + str(np.sum(czasy)) + " s")

        nazwaDozapisu = 'R'+ANN
        nazwaDozapisu = folderPO+'/'+nazwaDozapisu

        np.savetxt("TREN" + ANN[:-3] + ".csv", np.array(historLoss), delimiter=',')
        np.savetxt("CZAS" + ANN[:-3] + ".csv", np.array(czasy), delimiter=',')

        model.save(nazwaDozapisu)
        print("")
        print("ZAPISANO WYNIKI")
    return 0


Main()
